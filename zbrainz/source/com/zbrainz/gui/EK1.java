package com.zbrainz.gui;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import com.zbrainz.util.*;



public class EK1
{
    JFrame m_frame;
    ZPanel m_panel;
    com.zbrainz.gui.ZKeyBoard  m_KBgui;
    com.zbrainz.util.SoundBox  m_KBsound;
    boolean[]   m_keyState = new boolean[13];


    public static void main( String[] args )
    {
        ZLog.init();
        EK1 gui = new EK1();
        gui.go();
    }// main


    public void go ()
    {
        com.zbrainz.util.ALibUtil.enableDebug();
        m_frame = new JFrame();
        m_frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
        m_frame.setSize(900, 400);

        m_KBgui = new com.zbrainz.gui.ZKeyBoard(20, 30, 18, 100, 6);
        m_KBsound = new com.zbrainz.util.SoundBox(0);
        m_panel = new ZPanel();
        m_frame.add(m_panel);
        m_KBsound.Start();
        m_panel.setBackground(Color.YELLOW);
        m_panel.addMouseListener(m_panel);
        m_frame.addKeyListener(m_panel);
        m_frame.setVisible(true);
    }


    public class ZPanel extends JPanel implements MouseListener, KeyListener
    {
        public void paintComponent( Graphics g)
        {
            ZLog.logInfo("Hello");
            m_KBgui.draw(g);
        }


        public void mousePressed(MouseEvent e) 
        {
            int x = e.getX();
            int y = e.getY();
            int i = m_KBgui.convertMouseClickToKey(x, y);
            ZLog.logInfo("Key Press event");
            if ( i >= 0 )
            {
                m_KBgui.keyPressed(i);
                m_KBsound.PlayStart(24+i);
                ZLog.logInfo("Key Pressed[%d]\n", i);
            }
            m_frame.requestFocus();
            repaint();
        }

        public void mouseReleased(MouseEvent e) 
        {
            int x = e.getX();
            int y = e.getY();
            int i = m_KBgui.convertMouseClickToKey(x, y);
            ZLog.logInfo("Key release event");
            if ( i >= 0 )
            {
                m_KBgui.keyReleased(i);
                m_KBsound.PlayStop(24+i);
                ZLog.logInfo("Key Released[%d]\n", i);
            }
            m_frame.requestFocus();
            repaint();
        }

        public void mouseEntered(MouseEvent e) 
        {
            m_frame.requestFocus();
        }

        public void mouseExited(MouseEvent e) 
        {
            m_frame.requestFocus();
        }

        public void mouseClicked(MouseEvent e) 
        {
            m_frame.requestFocus();
        }

        public void  keyPressed(KeyEvent e)
        {
            int i = -1;
            char c = e.getKeyChar();
            c = Character.toLowerCase(c);
            ZLog.logInfo("KeyPressed Event : %c", c);
            if  (c == 'a') i=0;
            else if  (c == 'w') i=1;
            else if  (c == 's') i=2;
            else if  (c == 'e') i=3;
            else if  (c == 'd') i=4;
            else if  (c == 'h') i=5;
            else if  (c == 'u') i=6;
            else if  (c == 'j') i=7;
            else if  (c == 'i') i=8;
            else if  (c == 'k') i=9;
            else if  (c == 'o') i=10;
            else if  (c == 'l') i=11;
            else if  (c == ';') i=12;

            if ( i < 0  ) return;
            if ( m_keyState[i] == true ) { return; }
            this.requestFocus();
            m_KBgui.keyPressed(24+i);
            m_KBsound.PlayStart(48+i);
            m_keyState[i] = true;
            this.repaint();
            m_frame.requestFocus();
        }

        public void keyReleased(KeyEvent e)
        {
            int i = -1;
            char c = e.getKeyChar();
            c = Character.toLowerCase(c);
            ZLog.logInfo("KeyReleased Event : %c", c);
            if  (c == 'a') i=0;
            else if  (c == 'w') i=1;
            else if  (c == 's') i=2;
            else if  (c == 'e') i=3;
            else if  (c == 'd') i=4;
            else if  (c == 'h') i=5;
            else if  (c == 'u') i=6;
            else if  (c == 'j') i=7;
            else if  (c == 'i') i=8;
            else if  (c == 'k') i=9;
            else if  (c == 'o') i=10;
            else if  (c == 'l') i=11;
            else if  (c == ';') i=12;
            if ( i < 0  ) return;
            if ( m_keyState[i] == false ) { return; }

            m_frame.requestFocus();
            m_KBgui.keyReleased(24+i);
            m_KBsound.PlayStop(48+i);
            m_keyState[i] = false;
            this.repaint();
            m_frame.requestFocus();
        }

        public void keyTyped(KeyEvent e)
        {
            char c = e.getKeyChar();
            m_frame.requestFocus();
        }
    }
}

