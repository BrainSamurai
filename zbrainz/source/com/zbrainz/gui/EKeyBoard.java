package com.zbrainz.gui;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import com.zbrainz.util.*;
import javax.sound.midi.*;

public class EKeyBoard implements ActionListener
{
    JFrame m_frame;
    JPanel m_panel;
    String m_str;
    KB     kb;
    static String[] m_keynames = new String[13];

    JButton [] m_Keys = new JButton[13] ;
    boolean[]   m_KeyPressed = new boolean[13];
    com.zbrainz.util.SoundBox  piano;

    public static void main( String[] args )
    {
        ZLog.init();
        EKeyBoard gui = new EKeyBoard();
        SetUpKeyNames();
        gui.go();
    }// main

    public void go()
    {
        com.zbrainz.util.ALibUtil.enableDebug();
        m_frame = new JFrame();
        piano = new SoundBox(0);
        m_frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
        m_frame.setSize(900, 200);
        m_panel = new JPanel();
        m_panel.setLayout(new BoxLayout(m_panel, BoxLayout.X_AXIS));
        m_frame.setContentPane(m_panel);

        int iKey = 0;
        for ( iKey = 0; iKey < 13; iKey++)
        {
            String str;
            m_Keys[iKey] = com.zbrainz.util.AButtonFactory.makeJButton(null, m_keynames[iKey], 20, 180);

            m_Keys[iKey].setBackground( getBackgroundColor(iKey));
            m_Keys[iKey].setForeground( getForegroundColor(iKey));
            m_Keys[iKey].addActionListener(this);
            m_panel.add(m_Keys[iKey]);
            m_KeyPressed[iKey] = false;
        }// for


        kb = new KB();
        m_frame.addKeyListener(kb);
        m_frame.requestFocus();
        piano.Start();
        m_frame.setVisible(true);
    } // go

    public static void SetUpKeyNames()
    {
        m_keynames[0] ="C  (a)";
        m_keynames[1] ="C# (w)";
        m_keynames[2] ="D  (s)";
        m_keynames[3] ="D# (e)";
        m_keynames[4] ="E  (d)";
        m_keynames[5] ="F  (h)";
        m_keynames[6] ="F# (u)";
        m_keynames[7] ="G  (j)";
        m_keynames[8] ="G# (k)";
        m_keynames[9] ="A  (o)";
        m_keynames[10]="A# (l)";
        m_keynames[11]="B  (;)";
        m_keynames[12]="C  (p)";
    }

    public void actionPerformed( ActionEvent event)
    {
        int iKey = 0;
        for ( iKey = 0; iKey < 13; iKey++)
        {
            if ( event.getSource() == m_Keys[iKey] )
                Play(48+iKey, 8);
        } // for
        m_frame.requestFocus();
    } // Action peformed

    public void Play(int Key, int interval)
    {
        piano.Play(Key, interval);
    }

    public class KB implements KeyListener
    {
        private int getKey(char c) {
            int i;
            switch (c) {
                case 'a' : i=0; break;
                case 'w' : i=1; break;
                case 's' : i=2; break;
                case 'e' : i=3; break;
                case 'd' : i=4; break;
                case 'h' : i=5; break;
                case 'u' : i=6; break;
                case 'j' : i=7; break;
                case 'i' : i=8; break;
                case 'k' : i=9; break;
                case 'o' : i=10; break;
                case 'l' : i=11; break;
                case ';' : i=12; break;
                default : i=-1; break;
            }
            return i;
        }
        public void  keyPressed(KeyEvent e) 
        {
            int i = -1;
            char c = e.getKeyChar();
            ZLog.logInfo("KeyPressed Event : %c", c);
            c = Character.toLowerCase(c);
            i = getKey(c);
            if ( i < 0  ) return;
            if ( m_KeyPressed[i] == true ) return;

            m_Keys[i].setBackground(Color.YELLOW);
            ZLog.logInfo("KeyPressed Event : processing %c", c);
            m_KeyPressed[i] = true;
            piano.PlayStart(48+i);
            m_frame.requestFocus();
        }

        public void keyReleased(KeyEvent e)
        {
            int i = -1;
            char c = e.getKeyChar();
            ZLog.logInfo("KeyReleased Event : %c", c);
            c = Character.toLowerCase(c);
            i = getKey(c);
            if ( i < 0  ) return;

            m_KeyPressed[i] = false;
            piano.PlayStop(48+i);
            m_Keys[i].setBackground( getBackgroundColor(i));
            m_frame.requestFocus();
        }

        public void keyTyped(KeyEvent e)
        {
            char c = e.getKeyChar();
            m_frame.requestFocus();
        }
    } 

    public static Color getBackgroundColor(int iKey)
    {
        iKey = iKey % 12;
        iKey++;
        switch(iKey)
        {
            case 1:
            case 3:
            case 5:
            case 6:
            case 8:
            case 10:
            case 12:
                return Color.WHITE;
            default:
                return Color.BLACK;
        }
    }

    public static Color getForegroundColor(int iKey)
    {
        iKey = iKey % 12;
        iKey++;
        switch(iKey)
        {
            case 1:
            case 3:
            case 5:
            case 6:
            case 8:
            case 10:
            case 12:
                return Color.BLACK;
            default:
                return Color.WHITE;
        }
    }

}// EKeyBoard

