package com.zbrainz.gui;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import com.zbrainz.util.*;
import java.util.ArrayList;



public class ZKey
{
    ArrayList<com.zbrainz.gui.ZRectangle>  m_rects;
    Color       m_bg;
    boolean     m_pressed;

    ZKey()
    {
        m_rects = new  ArrayList<com.zbrainz.gui.ZRectangle>();
        m_pressed = false;
    }

    public void add(int fx, int fy, int fwd, int fht, Color fbg)
    {
        ZRectangle rect = new ZRectangle(fx, fy, fwd, fht, fbg); 
        m_rects.add(rect);
        m_bg = fbg;
    }

    public void draw( Graphics g ,  boolean Pressed)
    {
        for (int j=0; j < m_rects.size(); j++) 
        {
            ZRectangle rect = m_rects.get(j);
            if ( Pressed == true )
            {
                rect.draw(g, Color.GREEN, false);
            }
            else
                rect.draw(g, m_bg, true);

        }
    }

    public void KeyPressed()
    {
        m_pressed = true;
    }

    public void KeyReleased()
    {
        m_pressed = false;
    }

    public void draw(Graphics g)
    {
        this.draw(g, m_pressed);
    }

}

