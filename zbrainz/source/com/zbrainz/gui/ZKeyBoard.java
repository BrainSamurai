package com.zbrainz.gui;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import com.zbrainz.util.*;
import java.util.ArrayList;



public class ZKeyBoard
{
    ArrayList<com.zbrainz.gui.ZKey>  m_keys;
    com.zbrainz.util.SoundBox  piano;

    int m_ws;
    int m_hs;
    int m_noctaves;
    int m_fx;
    int m_fy;

    // keywidth must be a multiple of 3 && height a multiple of 2
    ZKeyBoard(int fx, int fy, int key_width, int key_height, int noctaves)
    {
        // Add an octave
        m_keys     = new ArrayList<com.zbrainz.gui.ZKey>();
        m_ws       = key_width/3;
        m_hs       = key_height/2;
        m_fx       = fx;
        m_fy       = fy;
        m_noctaves = noctaves;
        for ( int i = 0; i < noctaves; i++)
        {
            addOctave(fx+i*(7*key_width), fy, key_width, key_height);
        }
    }

    public void addOctave(int fx, int fy, int kw, int kh)
    {

        // we add all the keys by hand.
        ZKey c;
        int ws = kw / 3; // ws --> width scale
        int hs = kh / 2; // hs --> height scale

        c = new ZKey(); // C
        c.add(fx, fy, ws*2, hs, Color.WHITE);
        c.add(fx, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

        c = new ZKey(); // C#
        c.add(fx+ws*2, fy, 2*ws, hs,  Color.BLACK);
        m_keys.add(c);

        c = new ZKey(); // D
        c.add(fx+4*ws, fy, ws, hs, Color.WHITE);
        c.add(fx+3*ws, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

        c = new ZKey(); // D#
        c.add(fx+ws*5, fy, 2*ws, hs,  Color.BLACK);
        m_keys.add(c);

        c = new ZKey(); // E
        c.add(fx+7*ws, fy, ws*2, hs, Color.WHITE);
        c.add(fx+6*ws, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

        c = new ZKey(); // F
        c.add(fx+9*ws, fy, ws*2, hs, Color.WHITE);
        c.add(fx+9*ws, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

        c = new ZKey(); // F#
        c.add(fx+ws*11, fy, 2*ws, hs,  Color.BLACK);
        m_keys.add(c);

        c = new ZKey(); // G
        c.add(fx+13*ws, fy, ws, hs, Color.WHITE);
        c.add(fx+12*ws, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

        c = new ZKey(); // G#
        c.add(fx+ws*14, fy, 2*ws, hs,  Color.BLACK);
        m_keys.add(c);


        c = new ZKey(); // A
        c.add(fx+16*ws, fy, ws, hs, Color.WHITE);
        c.add(fx+15*ws, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

        c = new ZKey(); // A#
        c.add(fx+ws*17, fy, 2*ws, hs,  Color.BLACK);
        m_keys.add(c);

        c = new ZKey(); // B
        c.add(fx+19*ws, fy, 2*ws, hs, Color.WHITE);
        c.add(fx+18*ws, fy+hs, ws*3, hs, Color.WHITE);
        m_keys.add(c);

    }

    public void draw( Graphics g )
    {
        for (int j=0; j < m_keys.size(); j++) 
        {
            com.zbrainz.gui.ZKey key = m_keys.get(j);
            key.draw(g);
        }
    }

    public void keyPressed( int i)
    {
        if ( i >= (m_noctaves*12) ) return;
        m_keys.get(i).KeyPressed();
    }

    public void keyReleased( int i)
    {
        if ( i >= (m_noctaves*12) ) return;
        m_keys.get(i).KeyReleased();
    }

    public int convertMouseClickToKey(int x, int y)
    {

        x = x - m_fx;
        y = y - m_fy;
        System.out.format("x=%d, y = %d, fx=%d, fy=%d, m_noctaves=%d, m_ws=%d\n", x, y, m_fx, m_fy, m_noctaves, m_ws);

        if ( (x < 0)|| ( x>= 3*m_ws*7*(m_noctaves+1))) return -1;
        if ( (y < 0)|| ( y>= m_hs*2)) return -1;

        int noctaves = x / (7*m_ws*3);
        int rel_x   = x % (7*m_ws*3);
        int loc = rel_x / m_ws;

        System.out.format("noctaves = %d, rel_x = %d, loc = %d, y = %d\n", noctaves, rel_x, loc, y);


        switch ( loc )
        {
            case 0:
            case 1:
            case 2:
                {
                    if ( (loc == 2 ) && ( y < this.m_hs) )
                        return (noctaves*12)+1;
                    else
                        return noctaves*12; 
                }

            case 3:
            case 4:
            case 5:
                {
                    if ( (loc == 3 ) && ( y < this.m_hs) )
                        return (noctaves*12)+1;
                    if ( (loc == 5 ) && ( y < this.m_hs) )
                        return (noctaves*12)+3;
                    else
                        return noctaves*12+2; 
                }

            case 6:
            case 7:
            case 8:
                {
                    if ( (loc == 6 ) && ( y < this.m_hs) )
                        return (noctaves*12)+3;
                    else
                        return (noctaves*12)+4;
                }

            case 9:
            case 10:
            case 11:
                {
                    if ( (loc == 11 ) && ( y < this.m_hs) )
                        return (noctaves*12)+6;
                    else
                        return (noctaves*12)+5;
                }

            case 12:
            case 13:
            case 14:
                {
                    if ( (loc == 12 ) && ( y < this.m_hs) )
                        return (noctaves*12)+6;
                    if ( (loc == 14 ) && ( y < this.m_hs) )
                        return (noctaves*12)+8;
                    else
                        return (noctaves*12)+7;
                }

            case 15:
            case 16:
            case 17:
                {
                    if ( (loc == 15 ) && ( y < this.m_hs) )
                        return (noctaves*12)+8;
                    if ( (loc == 17 ) && ( y < this.m_hs) )
                        return (noctaves*12)+10;
                    else
                        return (noctaves*12)+9;
                }

            case 18:
            case 19:
            case 20:
                {
                    if ( (loc == 18 ) && ( y < this.m_hs) )
                        return (noctaves*12)+10;
                    else
                        return (noctaves*12)+11;
                }

            default:
                System.out.println("Error MousekeyInvalid");
                return -1;
        }
    }

}

