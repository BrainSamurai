package com.zbrainz.gui;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import com.zbrainz.util.*;



public class ZRectangle
{
    int    m_tx;
    int    m_ty;
    int    m_ht;
    int    m_wd;
    Color  m_bg;
    boolean m_raised;

    public ZRectangle()
    {
        m_tx = m_ty = m_ht = m_wd = -1;
    }

    public ZRectangle(int fx, int fy, int fwd, int fht, Color fbg)
    {
        m_tx = fx; m_wd = fwd;
        m_ty = fy; m_ht = fht;
        m_bg = fbg;
        m_raised = true;
    }

    public void setRectangle(int fx, int fy, int fwd, int fht, Color fbg)
    {
        m_tx = fx; m_wd = fwd;
        m_ty = fy; m_ht = fht;
        m_bg = fbg;
        m_raised = true;
    }

    public void setRaised(boolean rz){ m_raised = rz; }

    public void draw( Graphics g, Color bg, boolean fRaised )
    {
        g.setColor(bg);
        g.fill3DRect(m_tx, m_ty, m_wd, m_ht, fRaised); 
    }

    public void draw( Graphics g)
    {
        g.setColor(m_bg);
        g.fill3DRect(m_tx, m_ty, m_wd, m_ht, m_raised); 
    }
}

