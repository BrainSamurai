package com.zbrainz.util;
import javax.swing.*;
import java.awt.*;
import java.lang.*;


public class AButtonFactory 
{
    public static JButton makeJButton( String strURL, String strLabel, int size_x, int size_y)
    {
        JButton button;
        if ( strURL == null )
        {
            return new JButton( strLabel);
        }
        else
        {
            System.out.println(strURL);
            java.net.URL imageURL = AButtonFactory.class.getResource(strURL);
            if (imageURL != null)
            {
                ImageIcon icon;
                icon = new ImageIcon(imageURL);
                ImageIcon ic2 = new ImageIcon(com.zbrainz.util.ALibUtil.getScaledImage(icon.getImage(), size_x, size_y));
                return new JButton(ic2);
            }
            return new JButton( strLabel);
        }
    } // JButton 
}// AButton 

