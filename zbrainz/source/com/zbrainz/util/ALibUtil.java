package com.zbrainz.util;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.sound.midi.*;



// A collection of utility functions which are part of the AlibUtil object.
public class ALibUtil
{
    static int m_debug = 0;
    public static Image getScaledImage(Image srcImg, int w, int h)
    {
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();
        return resizedImg;
    }

    public static MidiEvent makeEvent(int comd, int chan, int one, int two, int tick)
    {
        MidiEvent event = null;
        try
        {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        }//try
        catch(Exception Ex) { Ex.printStackTrace();}
        return event;
    } // make Event

    public static void enableDebug(){ m_debug = 1; }
    public static void disableDebug(){ m_debug = 0; }
    public static void debugPrint( String str){ if ( m_debug == 1 ) System.out.println(str); }          
} // LibUtil
