package com.zbrainz.util;
import javax.sound.midi.*;
import java.awt.Color;

public class SoundBox
{
    Sequencer sequencer;
    Transmitter seqTrans;
    Synthesizer synth;
    Receiver synthRcvr;

    int m_velocity = 200;

    public SoundBox(int instrument)
    {
        try
        {
            sequencer = MidiSystem.getSequencer();
            seqTrans = sequencer.getTransmitter();
            synth = MidiSystem.getSynthesizer();
            synth.open();
            synthRcvr = synth.getReceiver();
            seqTrans.setReceiver(synthRcvr);
            sequencer.open();
            sequencer.setTempoInBPM(400);
        } // try
        catch(Exception Ex) { Ex.printStackTrace(); }
    } //contructor

    public void Start()
    {
        System.out.format("Sequencer : started\n");
    }

    public void Stop()
    {
    }

    public void Play(int key, int duration)
    {
        noteOn(1, key);
    } // Play

    public void PlayStart(int key)
    {
        noteOn(1, key);
    }

    public void PlayStop(int key)
    {
        noteOff(1, key);
    }

    public void noteOn(int chan, int key)
    {
        synth.getChannels()[chan].noteOn(key, m_velocity);
    }

    public void noteOff(int chan, int key)
    {
        synth.getChannels()[chan].noteOff(key);
    }
}// SoundBox
