package com.zbrainz.util;
import java.util.logging.*;

public class ZLog
{
    private static Handler ch = new ConsoleHandler();
    private static Formatter fmt = new ZLogFormatter();
    private static Logger logger = Logger.getLogger("");

    public static void init() {
        ch.setFormatter(fmt);
        // Hack: Remove the default handler. We should actually set the
        // default handler property
        Handler list[] = logger.getHandlers();
        for (int i=0; i < list.length; i++) {
            logger.removeHandler(list[i]);
        }
        // 

        logger.addHandler(ch);
        logger.setLevel(Level.ALL);
    }

    public static void log(Level lvl, String fmt, Object ...args) {
        String str = String.format(fmt, args);
        logger.log(lvl, str);
    }

    public static void logErr(String fmt, Object ...args) {
        log(Level.SEVERE, fmt, args);
    }
    public static void logWarn(String fmt, Object ...args) {
        log(Level.WARNING, fmt, args);
    }
    public static void logInfo(String fmt, Object ...args) {
        log(Level.INFO, fmt, args);
    }
    public static void logConfig(String fmt, Object ...args) {
        log(Level.CONFIG, fmt, args);
    }
    public static void logFine(String fmt, Object ...args) {
        log(Level.FINE, fmt, args);
    }

}
