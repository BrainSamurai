package com.zbrainz.util;
import java.util.logging.*;

public class ZLogFormatter extends Formatter {
    public synchronized String format (LogRecord record) {
        StringBuffer sb = new StringBuffer();

        sb.append(record.getLoggerName());
        sb.append(": ");
        sb.append(formatMessage(record));
        sb.append("\n");
        return sb.toString();
    }
}
